package database

import (
	"crypto/rand"

	"github.com/google/uuid"
	"github.com/mediocregopher/radix.v2/pool"
	"golang.org/x/crypto/bcrypt"
)

//Database represents a database
type Database struct {
	pool *pool.Pool
}

//Init initializes the database
func Init() (*Database, error) {
	d := new(Database)
	p, err := pool.New("tcp", "localhost:6379", 10)
	if err != nil {
		return nil, err
	}
	d.pool = p
	return d, nil
}

//Get gets an entry from the database
func (d *Database) Get(id *uuid.UUID) (map[string]string, error) {
	conn, err := d.pool.Get()
	defer d.pool.Put(conn)
	if err != nil {
		return nil, err
	}
	r := conn.Cmd("HGETALL", id.String())
	if r.Err != nil {
		return nil, err
	}
	res, err := r.Map()
	if err != nil {
		return nil, err
	}
	return res, nil
}

//Set sets an entry in the database
func (d *Database) Set(id *uuid.UUID, data map[string]string) error {
	conn, err := d.pool.Get()
	defer d.pool.Put(conn)
	if err != nil {
		return err
	}
	f := []interface{}{id.String()}
	for k, v := range data {
		f = append(f, k, v)
	}
	r := conn.Cmd("HMSET", f...)
	if r.Err != nil {
		return r.Err
	}
	return nil
}

//Delete deletes an entry from the database
func (d *Database) Delete(id *uuid.UUID) error {
	conn, err := d.pool.Get()
	defer d.pool.Put(conn)
	if err != nil {
		return err
	}
	r := conn.Cmd("HGET", id.String(), "type")
	if r.Err != nil {
		return r.Err
	}
	t, err := r.Str()
	if err != nil {
		return err
	}
	r = conn.Cmd("HGET", id.String(), t)
	if r.Err != nil {
		return r.Err
	}
	tid, err := r.Str()
	if err != nil {
		return err
	}
	r = conn.Cmd("DEL", id.String())
	if r.Err != nil {
		return r.Err
	}
	r = conn.Cmd("HDEL", t, tid)
	if r.Err != nil {
		return r.Err
	}
	return nil
}

//Lookup looks up a database UUID based on a data type and a second-order identifier
func (d *Database) Lookup(t string, m string) (*uuid.UUID, error) {
	conn, err := d.pool.Get()
	defer d.pool.Put(conn)
	if err != nil {
		return nil, err
	}
	r := conn.Cmd("HGET", t, m)
	if r.Err != nil {
		return nil, err
	}
	s, err := r.Str()
	if err != nil {
		return nil, err
	}
	i, err := uuid.Parse(s)
	if err != nil {
		return nil, err
	}
	return &i, nil
}

//BLookup looks up a database UUID based on a data type and a second-order binary identifier
func (d *Database) BLookup(t string, m []byte) (*uuid.UUID, error) {
	conn, err := d.pool.Get()
	defer d.pool.Put(conn)
	if err != nil {
		return nil, err
	}
	r := conn.Cmd("HGET", t, m)
	if r.Err != nil {
		return nil, err
	}
	s, err := r.Str()
	if err != nil {
		return nil, err
	}
	i, err := uuid.Parse(s)
	if err != nil {
		return nil, err
	}
	return &i, nil
}

//CreateUser creates a user
func (d *Database) CreateUser(user, pass string) (*uuid.UUID, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.MinCost)
	if err != nil {
		return nil, err
	}
	id := uuid.New()
	conn, err := d.pool.Get()
	defer d.pool.Put(conn)
	if err != nil {
		return nil, err
	}
	r := conn.Cmd("HMSET", id.String(), "user", user, "pass", hash, "state", "pending", "type", "user")
	if r.Err != nil {
		return nil, err
	}
	r = conn.Cmd("HSET", "user", user, id.String())
	if r.Err != nil {
		_ = conn.Cmd("DEL", id.String())
		return nil, err
	}
	return &id, nil
}

//MakeUserDeviceToken makes a user device token
func (d *Database) MakeUserDeviceToken(user string) (*uuid.UUID, []byte, error) {
	id := uuid.New()
	token := make([]byte, 256)
	_, err := rand.Read(token)
	if err != nil {
		return nil, nil, err
	}
	conn, err := d.pool.Get()
	defer d.pool.Put(conn)
	if err != nil {
		return nil, nil, err
	}
	r := conn.Cmd("HMSET", id.String(), "user", user, "token", token, "verified", false, "type", "token")
	if r.Err != nil {
		return nil, nil, err
	}
	r = conn.Cmd("HSET", "token", token, id.String())
	if r.Err != nil {
		_ = conn.Cmd("DEL", id.String())
		return nil, nil, err
	}
	return &id, token, nil
}

//CheckUser checks the validity of user credentials
func (d *Database) CheckUser(user, pass string) bool {
	return false
}
