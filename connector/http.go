package connector

import (
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strings"
	"sync"

	"gitlab.com/zenerboson/arbitrade/database"
	"gitlab.com/zenerboson/arbitrade/randstring"

	"github.com/golang/protobuf/proto"

	"gitlab.com/zenerboson/arbitrade/proto"

	"gitlab.com/zenerboson/arbitrade/exchanges"
)

type handler struct {
	exchanges *exchanges.Exchanges
	pRefs     *sync.Map
	cRefs     *sync.Map
	database  *database.Database
	tg        *tg
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if r.URL.Path != "/" {
		p := strings.Split(r.URL.Path[1:], "/")
		if len(p) != 2 {
			http.Error(w, "initialization failed", http.StatusInternalServerError)
			return
		}
		v, ok := h.pRefs.Load(p[0])
		if !ok {
			http.Error(w, "initialization failed", http.StatusInternalServerError)
			return
		}
		v.(*peer).Connect(p[1])
		h.pRefs.Delete(p[0])
		return
	}
	p, err := rtcNew()
	p.OnDisconnect(func() {
		if p.token != nil {
			if v, ok := h.cRefs.Load(p.token); ok && v.(*peer) == p {
				h.cRefs.Delete(p.token)
			}
		}
	})
	p.OnMessage(func(b []byte) {
		m := new(message.Request)
		if proto.Unmarshal(b, m) != nil {
			return
		}
		switch m.Action {
		case message.Action_WATCH:
			if m.Target == nil {
				return
			}
		case message.Action_REGISTER:
			if m.Credentials == nil {
				return
			}
			k, _ := regexp.MatchString("^[A-Za-z][A-Za-z0-9_]{2,31}$", m.Credentials.Username)
			if !k {
				return
			}
			k, _ = regexp.MatchString(".*[-$_@#^&*!\\./\\\\,?].*", m.Credentials.Password)
			i, _ := regexp.MatchString("[A-ZA-z0-9-$_@#^&*!\\./\\\\,?]{8,64}", m.Credentials.Password)
			j, _ := regexp.MatchString(".*\\d.*", m.Credentials.Password)
			z, _ := regexp.MatchString(".*[A-Z].*", m.Credentials.Password)
			if !k || !i || !j || !z {
				return
			}
			_, err := h.database.Lookup("user", m.Credentials.Username)
			if err == nil {
				p.SendMessage(&message.Response{
					Type: message.RespType_FAILURE,
					Data: &message.Response_Message{Message: "That username is already in use"},
				})
				return
			}
			id, err := h.database.CreateUser(m.Credentials.Username, m.Credentials.Password)
			if err != nil {
				p.SendMessage(&message.Response{
					Type: message.RespType_FAILURE,
					Data: &message.Response_Message{Message: "An internal server error occurred"},
				})
				return
			}
			p.user = id
			tid, token, err := h.database.MakeUserDeviceToken(m.Credentials.Username)
			p.token = tid
			h.cRefs.Store(p.token, p)
			if err != nil {
				p.SendMessage(&message.Response{
					Type: message.RespType_FAILURE,
					Data: &message.Response_Message{Message: "Device token creation failed"},
				})
				return
			}
			p.SendMessage(&message.Response{
				Type: message.RespType_SUCCESS,
				Data: &message.Response_BMessage{BMessage: token},
			})
		case message.Action_LOGIN:
			if p.user != nil {
				return
			}
			if m.Credentials != nil {
				if m.Credentials.Code != nil {
					id, err := h.database.BLookup("token", m.Credentials.Code)
					if err != nil {
						p.SendMessage(&message.Response{
							Type: message.RespType_FAILURE,
						})
					} else {
						token, err := h.database.Get(id)
						if err != nil {
							p.SendMessage(&message.Response{
								Type: message.RespType_FAILURE,
							})
							return
						}
						if v, ok := token["user"]; ok && v == m.Credentials.Username {
							p.token = id
							if v, ok := token["verified"]; ok && v == "1" {
								id, err := h.database.Lookup("user", m.Credentials.Username)
								if err != nil {
									p.SendMessage(&message.Response{
										Type: message.RespType_FAILURE,
									})
									return
								}
								p.user = id
								p.SendMessage(&message.Response{
									Type: message.RespType_SUCCESS,
									Data: &message.Response_User{User: &message.UserState{
										Verified: true,
									}},
								})
							} else {
								id, err := h.database.Lookup("user", m.Credentials.Username)
								if err != nil {
									p.SendMessage(&message.Response{
										Type: message.RespType_FAILURE,
									})
									return
								}
								p.user = id
								user, err := h.database.Get(id)
								if err != nil {
									p.SendMessage(&message.Response{
										Type: message.RespType_FAILURE,
									})
									return
								}
								us := ""
								if v, ok := user["state"]; ok {
									us = v
								}
								h.cRefs.Store(p.token, p)
								p.SendMessage(&message.Response{
									Type: message.RespType_SUCCESS,
									Data: &message.Response_User{User: &message.UserState{
										Verified:  false,
										UserState: us,
									}},
								})
							}
							return
						}
						p.SendMessage(&message.Response{
							Type: message.RespType_FAILURE,
						})
					}
				}
			}
		case message.Action_BEGIN_TG_VERIFY:
			if p.user == nil {
				return
			}
			if p.token == nil {
				return
			}
			user, err := h.database.Get(p.user)
			if err != nil {
				return
			}
			token, err := h.database.Get(p.token)
			if err != nil {
				return
			}
			if v, ok := user["state"]; ok && v == "verified" {
				if m, kk := token["verified"]; kk && m == "1" {
					return
				}
			}
			if p.vcode != "" {
				return
			}
			code, err := randstring.GenerateRandomString(6)
			if err != nil {
				return
			}
			vcode, err := randstring.GenerateRandomString(10)
			if err != nil {
				return
			}
			p.code = code
			p.vcode = vcode
			p.SendMessage(&message.Response{
				Type: message.RespType_SUCCESS,
				Data: &message.Response_User{User: &message.UserState{
					Verified:  false,
					UserState: "code",
				}},
			})
			h.tg.verify(p, user["user"])
		case message.Action_CANCEL:
			p.cancel()
			if p.tid != 0 {
				h.tg.psend(p, "You have cancelled the verification process. Your account will not be created.")
			}
			p.Disconnect()
			h.cRefs.Delete(p.token)
			user, err := h.database.Get(p.user)
			if err != nil {
				return
			}
			if v, ok := user["state"]; ok && v == "verified" {
				return
			}
			h.database.Delete(p.user)
			h.database.Delete(p.token)
			p.user = nil
			p.token = nil
		case message.Action_VERIFY:
			if p.vcode == "" || p.code == "" {
				return
			}
			if m.Credentials != nil {
				if m.Credentials.VerifyCode == p.code {
					p.SendMessage(&message.Response{
						Type: message.RespType_SUCCESS,
						Data: &message.Response_User{User: &message.UserState{
							Verified: true,
						}},
					})
					p.vcode = ""
					p.code = ""
					h.database.Set(p.user, map[string]string{
						"verified": "1",
						"state":    "verified",
					})
					h.tg.psend(p, fmt.Sprintf("Your account has successfully been verified. You will now be signed in."))
				} else {
					p.SendMessage(&message.Response{
						Type: message.RespType_FAILURE,
						Data: &message.Response_User{User: &message.UserState{
							Verified: false,
						}},
					})
					code, err := randstring.GenerateRandomString(6)
					if err != nil {
						return
					}
					h.tg.psend(p, fmt.Sprintf("Your confirmation code was entered incorrectly. A new code has been generated and is as follows: %s.", code))
					p.code = code
				}
			}
		}
	})
	if err != nil {
		http.Error(w, "initialization failed", http.StatusInternalServerError)
		return
	}
	h.pRefs.Store(p.SD, p)
	w.Write([]byte(p.SD))
}

func runHTTP(e *exchanges.Exchanges) {
	d, err := database.Init()
	if err != nil {
		panic(err)
	}
	t, err := initTG(d)
	if err != nil {
		log.Fatal(err)
	}
	h := &handler{
		exchanges: e,
		pRefs:     new(sync.Map),
		cRefs:     new(sync.Map),
		tg:        t,
		database:  d,
	}
	log.Fatal(http.ListenAndServe(":8081", h))
}
