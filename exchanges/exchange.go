package exchanges

//ExchangeStatus represents the status of an exchange
type ExchangeStatus struct {
	Trading bool
	Error   error
}

//Exchange represents an exchange
type Exchange interface {
	Name() string
	Status() ExchangeStatus
	Markets() []*Market
}

//Market represents the permissible actions on a pair of tradeable symbols within a specific exchange
type Market struct {
	Symbol   [2]string
	Trading  bool
	Exchange Exchange
}

//Exchanges represents the manager of all exchanges
type Exchanges struct {
	Exchanges []Exchange
}

//Setup sets up the exchange connectors
func Setup() *Exchanges {
	e := new(Exchanges)
	e.Exchanges = []Exchange{}
	e.initBinance()
	return e
}
