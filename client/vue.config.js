const path = require("path");

module.exports = {
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.proto$/,
          use: "file-loader"
        },
        {
          test: /\.(ttf|eot|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "file-loader"
        }
      ]
    },
    resolve: {
      alias: {
        $: path.resolve(__dirname, "../proto")
      }
    }
  },
  chainWebpack: config => {
    config.module.rules.delete("fonts");
  }
};
