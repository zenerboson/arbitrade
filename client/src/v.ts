import { Store } from "Vuex";
import Vue from "vue";
import Router from "vue-router";
import { AppState } from "./store";

export default abstract class V extends Vue {
  public $store!: Store<AppState>;
  public $router!: Router;
}

export enum Severity {
  Normal = 0,
  Positive,
  Warning,
  Error
}

export interface Toast {
  severity: Severity;
  content: string;
  ttl: number;
}

export function debounce(func: () => void, wait: number, immediate?: boolean) {
  let timeout: number | null;

  return function(this: any) {
    const context = this;
    const args = arguments;

    const later = () => {
      timeout = null;
      if (!immediate) {
        func.apply(context, args);
      }
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout!);

    timeout = setTimeout(later, wait);

    if (callNow) {
      func.apply(context, args);
    }
  };
}
