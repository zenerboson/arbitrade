package main

import (
	"gitlab.com/zenerboson/arbitrade/connector"
	"gitlab.com/zenerboson/arbitrade/exchanges"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	e := exchanges.Setup()
	connector.Run(e)
}
