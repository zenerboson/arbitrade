package connector

import (
	"github.com/google/uuid"
	"github.com/pions/webrtc"
	"gitlab.com/zenerboson/arbitrade/exchanges"
)

type peer struct {
	SD                 string
	connector          *webrtc.RTCPeerConnection
	channel            *webrtc.RTCDataChannel
	disconnectHandlers []func()
	onConnect          func()
	onMessage          func([]byte)
	dc                 chan []byte
	user               *uuid.UUID
	token              *uuid.UUID
	vtoken             *uuid.UUID
	code               string
	tid                int64
	vcode              string
	cancel             func()
}

func (p *peer) Disconnect() {
	for _, f := range p.disconnectHandlers {
		f()
	}
}

func (p *peer) OnDisconnect(f func()) {
	p.disconnectHandlers = append(p.disconnectHandlers, f)
}

//Run runs the connector
func Run(e *exchanges.Exchanges) {
	runHTTP(e)
}
