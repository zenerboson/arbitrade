package connector

import (
	"github.com/golang/protobuf/proto"
	"github.com/pions/webrtc"
	"github.com/pions/webrtc/examples/util"
	"github.com/pions/webrtc/pkg/datachannel"
	"github.com/pions/webrtc/pkg/ice"
	"gitlab.com/zenerboson/arbitrade/proto"
)

func rtcNew() (*peer, error) {
	p := new(peer)
	p.vcode = ""
	p.code = ""
	p.tid = 0
	p.cancel = func() {}
	config := webrtc.RTCConfiguration{
		IceServers: []webrtc.RTCIceServer{
			{
				URLs: []string{"stun:stun.l.google.com:19302"},
			},
		},
	}
	p.onMessage = func([]byte) {}
	p.onConnect = func() {}
	p.dc = make(chan []byte)

	c, err := webrtc.New(config)
	if err != nil {
		return nil, err
	}
	p.connector = c

	d, err := c.CreateDataChannel("data", nil)
	if err != nil {
		return nil, err
	}
	p.channel = d
	c.OnICEConnectionStateChange(func(connectionState ice.ConnectionState) {
		if connectionState == ice.ConnectionStateDisconnected {
			p.Disconnect()
		}
	})

	p.channel.OnOpen(func() {
		p.onConnect()
		for {
			b := <-p.dc
			p.channel.Send(datachannel.PayloadBinary{Data: b})
		}
	})

	p.channel.OnMessage(func(payload datachannel.Payload) {
		switch k := payload.(type) {
		case *datachannel.PayloadBinary:
			p.onMessage(k.Data)
		}
	})

	offer, err := c.CreateOffer(nil)
	if err != nil {
		return nil, err
	}

	p.SD = util.Encode(offer.Sdp)

	go func() {
		select {}
	}()

	return p, nil
}

func (p *peer) Connect(k string) error {
	sd := util.Decode(k)

	answer := webrtc.RTCSessionDescription{
		Type: webrtc.RTCSdpTypeAnswer,
		Sdp:  sd,
	}

	err := p.connector.SetRemoteDescription(answer)
	if err != nil {
		return err
	}
	return nil
}

func (p *peer) OnConnect(cb func()) {
	p.onConnect = cb
}

func (p *peer) OnMessage(cb func([]byte)) {
	p.onMessage = cb
}

func (p *peer) SendMessage(r *message.Response) {
	b, err := proto.Marshal(r)
	if err != nil {
		return
	}
	p.dc <- b
}
