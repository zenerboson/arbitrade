package connector

import (
	"fmt"
	"os"
	"sync"

	"gitlab.com/zenerboson/arbitrade/database"
	"gitlab.com/zenerboson/arbitrade/randstring"

	"gitlab.com/zenerboson/arbitrade/proto"

	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/google/uuid"
)

type state int

const (
	stateInit = iota
	stateWait
	stateCWait
)

type user struct {
	id    int64
	token *uuid.UUID
	user  *uuid.UUID
	peer  *peer
	state state
}

type tg struct {
	users    *sync.Map
	eusers   *sync.Map
	verifies *sync.Map
	bot      *tgbotapi.BotAPI
	db       *database.Database
}

func (t *tg) verify(p *peer, username string) {
	p.OnDisconnect(func() {
		t.verifies.Delete(username)
	})
	t.verifies.Store(username, p)
}

func (t *tg) associateToken(tid *uuid.UUID, tgid int) {
	var u *user
	if v, ok := t.users.Load(tgid); ok {
		v.(*user).token = tid
		u = v.(*user)
		t.users.Store(tgid, v)
		if u.token != nil {
			t.eusers.Delete(u.token)
		}
	} else {
		u = &user{
			token: tid,
			id:    int64(tgid),
			state: stateInit,
		}
		t.users.Store(tgid, u)
	}
	t.eusers.Store(u.token, u)
}

func (t *tg) send(id *uuid.UUID, message string) {
	if v, ok := t.eusers.Load(id); ok {
		t.bot.Send(tgbotapi.NewMessage(v.(*user).id, message))
	}
}

func (t *tg) psend(peer *peer, message string) {
	t.bot.Send(tgbotapi.NewMessage(peer.tid, message))
}

func initTG(db *database.Database) (*tg, error) {
	t := new(tg)
	t.db = db
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TGKEY"))
	if err != nil {
		return nil, err
	}
	t.bot = bot
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)
	t.users = new(sync.Map)
	t.verifies = new(sync.Map)
	if err != nil {
		return nil, err
	}
	go func() {
		for update := range updates {
			if update.Message == nil {
				continue
			}
			if update.Message.IsCommand() {
				switch update.Message.Command() {
				case "start":
					if _, ok := t.users.Load(update.Message.From.ID); !ok {
						bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "What's the username for your pending account?"))
					}
					t.users.Store(update.Message.From.ID, &user{
						id:    int64(update.Message.From.ID),
						state: stateInit,
					})
				}
			} else {
				var u *user
				if v, ok := t.users.Load(update.Message.From.ID); !ok || v == nil {
					u = &user{
						id:    int64(update.Message.From.ID),
						state: stateInit,
					}
					t.users.Store(update.Message.From.ID, u)
				} else {
					u = v.(*user)
				}
				if u.state == stateInit {
					if v, ok := t.verifies.Load(update.Message.Text); ok {
						bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Please provide the confirmation code displayed on the registration page to continue."))
						t.users.Store(update.Message.From.ID, &user{
							id:    int64(update.Message.From.ID),
							state: stateWait,
							peer:  v.(*peer),
						})
						v.(*peer).SendMessage(&message.Response{
							Type: message.RespType_SUCCESS,
							Data: &message.Response_Message{Message: v.(*peer).vcode},
						})
						v.(*peer).cancel = func() {
							t.users.Store(update.Message.From.ID, &user{
								id:    int64(update.Message.From.ID),
								state: stateInit,
							})
						}
						t.verifies.Delete(update.Message.Text)
					} else {
						bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Sorry, I don't see a pending account with that name. Revisit your Arbitrade invite link and try again, then send me your username and I'll help verify your account."))
					}
				} else if u.state == stateWait {
					if p := u.peer; p != nil {
						if update.Message.Text == p.vcode {
							bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("Return to the registration page and enter the following code in the spaces provided to complete verification and sign in: %s.", p.code)))
							u.state = stateCWait
							p.SendMessage(&message.Response{
								Type: message.RespType_SUCCESS,
							})
							p.tid = update.Message.Chat.ID
						} else {
							bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Sorry, that verification code is incorrect. A new code has been generated, please provide it here."))
							vcode, err := randstring.GenerateRandomString(10)
							if err != nil {
								return
							}
							p.vcode = vcode
							p.SendMessage(&message.Response{
								Type: message.RespType_FAILURE,
								Data: &message.Response_Message{Message: p.vcode},
							})
						}
					}
				}

			}
		}
	}()
	return t, nil
}
