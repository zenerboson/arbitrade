import Vue from "vue";
import Vuex from "vuex";
import protobuf from "protobufjs";
import RTC from "./rtc";

let request: protobuf.Type;
let response: protobuf.Type;

interface Request {
  action: number;
  credentials?: Credentials;
  target?: Target;
}

interface Response {
  type: number;
  data: string;
  message: string;
  exchange: Exchange;
  bMessage: ArrayBuffer;
  user: UserState;
}

interface UserState {
  verified: boolean;
  userState: string;
}

interface Exchange {
  name: string;
}
interface Target {
  type?: number;
  all?: boolean;
  identifier?: number[];
}

Vue.use(Vuex);

export interface AppState {
  exchanges: Exchange[];
  authenticated: boolean;
  connected: boolean;
  error: string;
  token: Uint8Array | undefined;
  username: string;
  code: string;
}

interface Credentials {
  username?: string;
  password?: string;
  code?: Uint8Array;
  verifyCode?: string;
}

const store = new Vuex.Store<AppState>({
  state: {
    exchanges: [] as Exchange[],
    authenticated: false,
    connected: false,
    error: "",
    token: undefined,
    username: "",
    code: ""
  },
  mutations: {
    ADD_EXCHANGE(state, exchange) {
      state.exchanges.push(exchange);
    },
    CONNECTED(state) {
      state.connected = true;
    },
    DISCONNECTED(state) {
      state.connected = false;
    },
    SET_ERROR(state, error: string) {
      state.error = error;
    },
    CLEAR_ERROR(state) {
      state.error = "";
    },
    SET_TOKEN(state, token: Uint8Array) {
      state.token = token;
    },
    CLEAR_TOKEN(state) {
      state.token = undefined;
      localStorage.removeItem("token");
      localStorage.removeItem("user");
    },
    SET_USERNAME(state, username: string) {
      state.username = username;
    },
    SET_CODE(state, code: string) {
      state.code = code;
    },
    AUTHENTICATED(state) {
      state.authenticated = true;
    }
  },
  actions: {
    get_all(context, type: number) {
      send({
        action: 1,
        target: {
          all: true
        }
      });
    },
    verify_finalize(context, code) {
      send({
        action: 7,
        credentials: { verifyCode: code }
      });
    },
    tg_register(context) {
      send({
        action: 5
      });
    },
    register(context, credentials: Credentials) {
      context.commit("SET_USERNAME", credentials.username);
      send({
        action: 4,
        credentials
      });
    },
    handle_response(context, resp: Response) {
      if (
        context.state.code !== "" &&
        resp.user &&
        !resp.user.verified &&
        resp.type === 2
      ) {
        return;
      }
      if (resp.user && resp.user.verified) {
        context.commit("AUTHENTICATED");
        return;
      }
      if (context.state.code !== "" && resp.type === 2 && resp.message) {
        context.commit("SET_CODE", resp.message);
        return;
      }
      if (
        context.state.code !== "" &&
        resp.type === 1 &&
        !context.state.authenticated
      ) {
        context.commit("SET_ERROR", "ccode");
        return;
      }
      if (!context.state.authenticated && context.state.token) {
        if (resp.type === 2) {
          context.commit("CLEAR_TOKEN");
        } else {
          if (!localStorage.getItem("user")) {
            context.commit("SET_USERNAME", localStorage.getItem("user"));
          }
          if (!resp.user && context.state.error === "code") {
            if (resp.message) {
              context.commit("SET_CODE", resp.message);
              return;
            }
          }
          if (!resp.user.verified && resp.user.userState === "pending") {
            context.commit("SET_ERROR", "pending");
          }
          if (!resp.user.verified && resp.user.userState === "code") {
            context.commit("SET_ERROR", "code");
            send({
              action: 5
            });
          }
        }
        return;
      }
      if (!context.state.authenticated && resp.type === 2) {
        context.commit("SET_ERROR", resp.message);
      }
      if (!context.state.authenticated && resp.type === 1) {
        context.commit("SET_ERROR", "success");
        localStorage.setItem("user", context.state.username);
        localStorage.setItem(
          "token",
          btoa(String.fromCharCode(...new Uint8Array(resp.bMessage)))
        );
        context.commit("SET_TOKEN", new Uint8Array(resp.bMessage));
      }
    },
    restore_token(
      context,
      token: {
        token: string;
        resolve: (value?: {} | PromiseLike<{}> | undefined) => void;
      }
    ) {
      context.commit(
        "SET_TOKEN",
        new Uint8Array([...atob(token.token)].map(char => char.charCodeAt(0)))
      );
      send({
        action: 3,
        credentials: {
          code: context.state.token!,
          username: localStorage.getItem("user")!
        }
      });
      token.resolve();
    },
    cancel(context) {
      context.commit("CLEAR_ERROR");
      context.commit("CLEAR_TOKEN");
      send({
        action: 8
      });
      localStorage.removeItem("user");
    }
  },
  getters: {
    ready: state => {
      return ready;
    }
  }
});

let connector: RTC;

const ready = new Promise((resolve, reject) =>
  fetch(`http://${window.location.hostname}:8081`)
    .then(resp => resp.text())
    .then(sd => {
      protobuf.load(require("../../proto/message.proto")).then(root => {
        request = root.lookupType("Request");
        response = root.lookupType("Response");
        connector = new RTC(sd, (dd: string) => {
          fetch(`http://${window.location.hostname}:8081/${sd}/${dd}`).catch(
            () => resolve()
          );
        });
        connector.onMessage(message => {
          store.dispatch("handle_response", (response.decode(
            new Uint8Array(message)
          ) as unknown) as Response);
        });
        connector.onReady(() => {
          const token = localStorage.getItem("token");
          store.commit("CONNECTED");
          if (token) {
            store.dispatch("restore_token", { token, resolve });
          } else {
            resolve();
          }
        });
        connector.onDisconnect(() => {
          store.commit("DISCONNECTED");
        });
      });
    })
    .catch(() => {
      resolve();
    })
);

const send = (msg: Request) => {
  connector.sendMessage(request.encode(msg).finish());
};

export default store;
