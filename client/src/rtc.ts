export default class RTC {
  private pc: RTCPeerConnection;
  private sc: RTCDataChannel | undefined;
  private sd: string;
  private readyCallback: (() => void);
  private disconnectCallback: (() => void);
  private messageCallback: ((m: Uint8Array) => void);
  constructor(sd: string, cb: (sd: string) => void) {
    this.pc = new RTCPeerConnection();
    this.sd = "";
    this.readyCallback = () => undefined;
    this.disconnectCallback = () => undefined;
    this.messageCallback = (m: Uint8Array) => undefined;
    this.pc.oniceconnectionstatechange = (e) => {
      if (this.pc.iceConnectionState === "failed") {
        this.disconnectCallback();
      }
    };
    this.pc.onicecandidate = (event) => {
      if (event.candidate === null) {
        this.sd = btoa(this.pc.localDescription!.sdp);
        cb(this.sd);
      }
    };
    this.pc.ondatachannel = (e) => {
      this.sc = e.channel;
      this.sc.onclose = () => undefined;
      this.sc.onopen = () => {
        this.readyCallback!();
      };
      this.sc.onmessage = (a) => {
        this.messageCallback(a.data);
      };
    };
    this.pc.setRemoteDescription(
      new RTCSessionDescription({ type: "offer", sdp: atob(sd) })
    );
    this.pc.createAnswer().then((d) => this.pc.setLocalDescription(d));
  }
  public sendMessage(message: Uint8Array) {
    this.sc!.send(message);
  }
  public onReady(cb: () => void) {
    this.readyCallback = cb;
  }
  public onDisconnect(cb: () => void) {
    this.disconnectCallback = cb;
  }
  public onMessage(cb: (message: Uint8Array) => void) {
    this.messageCallback = cb;
  }
}
