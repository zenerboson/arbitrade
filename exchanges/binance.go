package exchanges

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type binanceSymbol struct {
	Symbol             string   `json:"symbol"`
	Status             string   `json:"status"`
	BaseAsset          string   `json:"baseAsset"`
	BaseAssetPrecision int      `json:"baseAssetPrecision"`
	QuoteAsset         string   `json:"quoteAsset"`
	QuotePrecision     int      `json:"quotePrecision"`
	OrderTypes         []string `json:"orderTypes"`
	IcebergAllowed     bool     `json:"icebergAllowed"`
}

type binanceExchangeInfo struct {
	Timezone   string          `json:"timezone"`
	ServerTime int             `json:"serverTime"`
	Symbols    []binanceSymbol `json:"symbols"`
}

type binance struct {
	exchangeInfo *binanceExchangeInfo
	status       ExchangeStatus
	markets      []*Market
}

func (b *binance) Markets() []*Market {
	return b.markets
}

func (b *binance) Name() string {
	return "Binance"
}

func (b *binance) Status() ExchangeStatus {
	return b.status
}

func (e *Exchanges) initBinance() {
	response, err := http.Get("https://api.binance.com/api/v1/exchangeInfo")
	if err != nil {
		e.Exchanges = append(e.Exchanges, &binance{
			status: ExchangeStatus{
				Trading: false,
				Error:   err,
			},
		})
		return
	}
	d, _ := ioutil.ReadAll(response.Body)
	ei := new(binanceExchangeInfo)
	err = json.Unmarshal(d, ei)
	if err != nil {
		e.Exchanges = append(e.Exchanges, &binance{
			status: ExchangeStatus{
				Trading: false,
				Error:   err,
			},
		})
		return
	}
	k := new(binance)
	k.status = ExchangeStatus{
		Trading: true,
	}
	k.exchangeInfo = ei
	for _, s := range ei.Symbols {
		k.markets = append(k.markets, &Market{
			Trading:  s.Status == "TRADING",
			Exchange: k,
			Symbol:   [2]string{s.BaseAsset, s.QuoteAsset},
		})
	}
	e.Exchanges = append(e.Exchanges, k)
}
